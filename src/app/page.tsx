'use client';

import { useState } from 'react';
import clx from 'classnames';

import { writeFile, removeTxtFiles } from './actions';

export default function Home() {
  const [lastFileName, setLastFileName] = useState('');
  const [text, setText] = useState('');
  const [fileName, setFileName] = useState('');

  const isValidFieldValue = (value: string) => {
    if (!value.trim().length && value.trim().length !== value.length) {
      return false;
    }
    return true;
  };

  const writeFileAction = async (formData: FormData) => {
    await writeFile(formData);
    setLastFileName(fileName);
    setText('');
    setFileName('');
    formData.set('file', '');
  };

  const removeFilesAction = async () => {
    setLastFileName('');
    await removeTxtFiles();
  };

  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <form action={writeFileAction} className="w-[400px] flex flex-col items-center justify-center">
        <label htmlFor="file" className="mr-auto mb-1">
          File name
        </label>
        <input
          id="file"
          type="text"
          name="file"
          min={1}
          max={150}
          placeholder="Please, type file name here..."
          className={clx(
            'w-full p-2 text-sm text-neutral-800 mb-4 rounded',
            isValidFieldValue(fileName) ? 'bg-neutral-000' : 'bg-red-100'
          )}
          value={fileName}
          onChange={(e) => setFileName(e.target.value)}
        />
        <label htmlFor="text" className="mr-auto mb-1">
          Your text
        </label>
        <input
          id="text"
          type="text"
          name="text"
          min={1}
          max={150}
          placeholder="Please, type your text here..."
          className={clx(
            'w-full p-2 text-sm text-neutral-800 mb-4 rounded',
            isValidFieldValue(text) ? 'bg-neutral-000' : 'bg-red-100'
          )}
          value={text}
          onChange={(e) => setText(e.target.value)}
        />
        <button
          className="mb-2 px-2 py-3 bg-green-300 text-neutral-700 hover:bg-green-500 transition duration-300 w-[200px] rounded border border-green-600 disabled:opacity-60 disabled:hover:bg-green-300"
          disabled={!isValidFieldValue(text) || !isValidFieldValue(fileName) || text.length === 0}
        >
          Write File
        </button>
        {lastFileName !== '' && (
          <a
            download
            href={`/${lastFileName}.txt`}
            className="mb-2 text-neutral-000 hover:text-neutral-200 underline transition duration-300"
          >
            Download {lastFileName}.txt
          </a>
        )}

        <button
          formAction={removeFilesAction}
          className="px-2 py-3 bg-red-300 text-neutral-700 hover:bg-red-400 transition duration-300 w-[200px] rounded border border-red-600 disabled:opacity-60 disabled:hover:bg-red-300"
        >
          Clear All .txt files
        </button>
      </form>
    </main>
  );
}
