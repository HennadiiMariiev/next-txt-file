'use server';
import fs from 'fs/promises';
import path from 'path';
// import fimraf from ''
 
export async function writeFile(formData: FormData) {
  try {
    const file_name = formData.get('file')?.toString();
    const text = formData.get('text')?.toString();

    const path = `public/${file_name || "text"}.txt`;
    return await fs.writeFile(path, text as string);
  } catch (error) {
    console.log(error);
    return null;
  }
}

export async function removeTxtFiles() {
  const directory = 'public';
  const removedFiles: string[] = [];

  try {
    
  for (const file of await fs.readdir(directory)) {
    removedFiles.push(file);
    await fs.unlink(path.join(directory, file));
  }
  } catch (error) {
    console.log(error);
    return [];
  }

  return removedFiles;
}