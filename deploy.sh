#!/usr/bin/env bash


export PATH=/home/ubuntu/.nvm/versions/node/v16.19.1/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin

git pull origin main
npm install
npm run build